/**
 * Created by isild on 06.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        String s = "First string";
        System.out.println("s:"+s.hashCode());

        String s2 = "Second string";
        System.out.println("s:"+s2.hashCode());

        String s3 = "First string";
        System.out.println("s:" + s3.hashCode());

        String s4 = s3;
        System.out.println("s:"+s4.hashCode());

        String s5 = new String("Second string");
         System.out.println("s:"+s5.hashCode());

        String s6=new String();
        System.out.println("s:"+s6.hashCode());

        String s7="";
        System.out.println("s:"+s7.hashCode());

        System.out.println(s2==s5);
        System.out.println(s.endsWith(s3));

        System.out.println(s5.intern());

        s5.isEmpty();

        char[] data=new char[10];
        data[0]='a';
        data[1]='b';
        data[9]='c';
       String s9= String.copyValueOf(data);
        System.out.println(s9);
        System.out.println(s6.length());






    }
}
